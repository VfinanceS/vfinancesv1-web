angular.module("lams").controller(
				"applicationListCtrl",
				[
						"$scope", "masterService", "$rootScope", "Notification", "applicationService", "Constant", "$filter", "$state", "$uibModal","userService",
						function($scope, masterService, $rootScope, Notification, applicationService, Constant, $filter, $state, $uibModal, userService) {
							$scope.showExistingApplication = false;
							$scope.showCurrentApplication = false;
							$scope.showClosedApplication = false;

							$scope.loanCategoryList = [ {
								id : 0,
								name : "New"
							}, {
								id : 1,
								name : "Balance Transfer"
							} ];

							$scope.acceptedStatus = Constant.Status.ACCEPTED;

							$scope.existingObj = {};
							$scope.currentObj = {};
							$scope.closedObj = {};

							$scope.existingAppCount = 0;
							$scope.curentAppCount = 0;
							$scope.closedAppCount = 0;

							$scope.applicationList = [];
							$scope.preApplicationList = [];
							$scope.coApplicantList = [];
							$scope.applicationTypeList = [];
							$scope.loanTypeList = [];

							$scope.totalExistingLoanAmount = 0;
							$scope.totalExistingLoanEMI = 0;
							$scope.getApplications = function() {
								$scope.totalExistingLoanAmount = 0;
								applicationService
										.getAll()
										.then(
												function(success) {
													if (success.data.status == 200) {
														$scope.existingLoanList = [];
														$scope.applicationList = success.data.data[0];
														$scope.coApplicantList = success.data.data[1];
														$scope.preApplicationList = success.data.data[2];

														$scope.selectedCoApplicant = $scope.coApplicantList[0];
														$scope.existingLoanList = $filter(
																'filter')
																(
																		$scope.applicationList,
																		{
																			loanTypeId : Constant.LoanType.EXISTING_LOAN
																		});
														for (var i = 0; i < $scope.existingLoanList.length; i++) {
															if (!$rootScope
																	.isEmpty($scope.existingLoanList[i].loanAmount)) {
																$scope.totalExistingLoanAmount = $scope.totalExistingLoanAmount
																		+ parseFloat($scope.existingLoanList[i].loanAmount);
																$scope.totalExistingLoanEMI = $scope.totalExistingLoanEMI
																		+ parseFloat($scope.existingLoanList[i].emi);
															}
														}
														$scope.existingAppCount = $scope.existingLoanList.length;
														$scope.curentAppCount = $filter(
																'filter')
																(
																		$scope.applicationList,
																		{
																			loanTypeId : Constant.LoanType.CURRENT_LOAN
																		}).length;
														$scope.closedAppCount = $filter(
																'filter')
																(
																		$scope.applicationList,
																		{
																			loanTypeId : Constant.LoanType.CLOSED_LOAN
																		}).length;
													} else {
														Notification
																.warning(success.data.message);
													}
												},
												function(error) {
													$rootScope.validateErrorResponse(error);
												});
							}
							$scope.getApplications();

							$scope.accept = function(application) {
								$scope.totalExistingLoanAmount = 0;
								applicationService
										.getAll()
										.then(
												function(success) {
													if (success.data.status == 200) {

													} else {
														Notification.warning(success.data.message);
													}
												},
												function(error) {
													$rootScope.validateErrorResponse(error);
												});
							}

							$scope.hideDiv = function(type, ev) {

								if (!$rootScope.isEmpty($rootScope.user)) {
									if ($rootScope
											.isEmpty($rootScope.user.isProfileFilled)
											|| !$rootScope.user.isProfileFilled) {
										$state.go("web.lams.brProfile");
										Notification.warning("Please Fill First Your Profile");
										return;
									}
								}

								$scope.appObj = {};
								if (type == Constant.LoanType.EXISTING_LOAN) {
									$scope.existingObj = {};
									$scope.showExistingApplication = !$scope.showExistingApplication;
								} else if (type == Constant.LoanType.CURRENT_LOAN) {
									$scope.currentObj = {};
									$scope.showCurrentApplication = !$scope.showCurrentApplication;
								} else if (type == Constant.LoanType.CLOSED_LOAN) {
									$scope.closedObj = {};
									$scope.showClosedApplication = !$scope.showClosedApplication;
								}
							}

							$scope.openModal = function(selected) {
								if ($scope.currentObj.coApplicantUserId == 5) {
									$scope.modalInstance = $uibModal
											.open({
												ariaLabelledBy : 'modal-title',
												ariaDescribedBy : 'modal-body',
												templateUrl : 'application/coApplicantProfile.html',
												controller : 'coApplicantProfileCtrl',
												controllerAs : '$ctrl',
												size : 'lg',
												resolve : {}
											}).result
											.then(
													function() {
														console
																.log("Modal is dimissed !");
													},
													function(res) {
														$scope.currentObj.coApplicantUserId = {};
													});
								}
							}

							$rootScope.$on('refresh-coapplicantList', function(
									event, data) {
								$scope.coApplicantList = data;
							});

							$scope.existingLoanSave = function() {
								if ($scope.existingForm.$invalid) {
									$scope.existingForm.$submitted = true;
									Notification
											.warning("Please fill all mandatory fields !!");
									return;
								}
								$scope.saveApplication($scope.existingObj,
										Constant.LoanType.EXISTING_LOAN);
							}
							
							$scope.validatePromoTionalCode = function() {
								
								if($scope.currentObj.promoCode =='' || $scope.currentObj.promoCode == null){
									$scope.currentLoanSave();
								}
								else{
									applicationService
									.validatePromoTionalCode($scope.currentObj.promoCode, $scope.currentObj.applicationTypeId)
									.then(
											function(success) {
												if (success.data.status == 200) {
													if (success.data.data) {
														$scope.currentLoanSave();
													} else {
														Notification.warning("Invalid Coupon Code, Please enter valid coupon code !!");
														return;
													}
												}
											},
											function(error) {
												console.log("Get me error : "+ error);
												;
											});
								}
							}
							
							$scope.currentLoanSave = function() {

								if ($scope.currentForm.$invalid) {
									$scope.currentForm.$submitted = true;
									Notification
											.warning("Please fill all mandatory fields !!");
									return;
								}

								applicationService
										.check30DaysGapFromLastTransaction()
										.then(
												function(success) {
													if (success.data.status == 200) {
														if (success.data.data) {
															$scope
																	.pleaseCheckDetailsForGapOf30Days();
														} else {
															$scope
																	.saveApplication(
																			$scope.currentObj,
																			Constant.LoanType.CURRENT_LOAN);
														}
													}
												},
												function(error) {
													console.log("Get me error : "+ error);
													;
												});

								// $scope.saveApplication($scope.currentObj,Constant.LoanType.CURRENT_LOAN);
							}
							$scope.closedLoanSave = function() {
								if ($scope.closedForm.$invalid) {
									$scope.closedForm.$submitted = true;
									Notification
											.warning("Please fill all mandatory fields !!");
									return;
								}
								$scope.saveApplication($scope.closedObj,
										Constant.LoanType.CLOSED_LOAN);
							}

							$scope.saveApplication = function(appObj, type) {
								var data = {};
								data.applicationTypeId = appObj.applicationTypeId;
								data.coApplicantId = appObj.coApplicantUserId;
								appObj.coApplicantId = appObj.coApplicantUserId;
								data.loanCategoryId = appObj.loanCategoryId;
								appObj.loanTypeId = type;
								// data.status = "OPEN";//loanCategoryId
								data.data = JSON.stringify(appObj);
								applicationService
										.save(data)
										.then(
												function(success) {
													if (success.data.status == 200) {
														Notification.info("Co-Applicant Created  Successfully!!");
														if (type == Constant.LoanType.EXISTING_LOAN
																|| type == Constant.LoanType.CLOSED_LOAN) {
															$scope.getApplications();
															$scope.hideDiv(type);
														} else {
															$state.go(
																			"web.lams.application",
																			{
																				mode : Constant.mode.EDIT,
																				appCode : $rootScope
																						.getAppIdByAppCode(appObj.applicationTypeId),
																				appId : success.data.data
																			});
														}

													} else {
														Notification.warning(success.data.message);
													}
												},
												function(error) {
													$rootScope.validateErrorResponse(error);
												});
							}
							
							//
							$scope.confirmPreApplicationByBorrower = function(appObj) {
								
								var data = {};
								data.id = appObj.id;
								data.applicationTypeId=appObj.applicationTypeId;
								data.channelPartnerId=appObj.channelPartnerId;
								
								data.data = JSON.stringify(appObj);
								
								userService.saveOrUpdateBorrowerByCPLatest(data).then(
							            function(success) {
							            	$scope.isDisable = false;
							            	console.log("success.data==============>",success.data);
							            	if(success.data.status == 200){
							            		Notification.info("Application Approved Successfully!");
							            		$scope.preApplicationList = success.data.data;
							            		$scope.getApplications();
							            	}else{
							                	Notification.warning(success.data.message);
							                }
							            }, function(error) {
							            	$scope.isDisable = false;
							            	$rootScope.validateErrorResponse(error);
							     });
							}

							$scope.deleteApplication = function(applicationId) {
								if ($rootScope.isEmpty(applicationId)) {
									Notification
											.warning("Something is Wrong! Please try to Refresh Page or Relogin!");
									return false;
								}

								applicationService
										.inactive(applicationId)
										.then(
												function(success) {
													if (success.data.status == 200) {
														Notification.info("Application Deleted Successfully!");
														$scope.getApplications();
													} else {
														Notification.warning(success.data.message);
													}
												},
												function(error) {
													$rootScope
															.validateErrorResponse(error);
												});
							}

							function DialogController($scope, $mdDialog) {
								$scope.hide = function() {
									$mdDialog.hide();
								};

								$scope.cancel = function() {
									$mdDialog.cancel();
								};

								$scope.answer = function(answer) {
									$mdDialog.hide(answer);
								};
							}

							$scope.getApplicationType = function() {
								if ($scope.applicationTypeList.length > 0) {
									return;
								}
								masterService
										.applicationType(-1)
										.then(
												function(success) {
													if (success.data.status == 200) {
														$scope.applicationTypeList = success.data.data;

													} else {
														Notification
																.warning(success.data.message);
													}
												},
												function(error) {
													$rootScope
															.validateErrorResponse(error);
												});
							}
							$scope.getApplicationType();

							$scope.getLoanType = function() {
								if ($scope.loanTypeList.length > 0) {
									return;
								}
								masterService
										.loanType(0)
										.then(
												function(success) {
													if (success.data.status == 200) {
														$scope.loanTypeList = success.data.data;
													} else {
														Notification
																.warning(success.data.message);
													}
												},
												function(error) {
													$rootScope
															.validateErrorResponse(error);
												});
							}
							$scope.getLoanType();

							$scope.pleaseCheckDetailsForGapOf30Days = function() {
								var message = "Are you sure want to continue ? Please check the details again. ";

								var modalHtml = '<div class="modal-body">'
										+ message + '</div>';
								modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

								$scope.modalInstanceConfirm = $uibModal.open({
									template : modalHtml,
									controller : ModalInstanceCtrl
								});

								$scope.modalInstanceConfirm.result.then(function() {
									$scope.saveApplication($scope.currentObj, Constant.LoanType.CURRENT_LOAN);
								});
							};

							$scope.acceptChannelPartnerOffer = function(item) {
								var message = "Are you sure ?";

								var modalHtml = '<div class="modal-body"> you want to accept this offer.</div>';
								modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

								$scope.modalInstanceAcceptChPrtnOffer = $uibModal.open({
									template : modalHtml,
									controller : ModalInstanceAcceptCtrl
								});

								$scope.modalInstanceAcceptChPrtnOffer.result.then(function() {
									$scope.confirmPreApplicationByBorrower(item);
								});
							};

						} ]);

angular.module("lams").controller(
				"coApplicantProfileCtrl",
				function($scope, $http, $rootScope, Constant, userService,
						Notification, masterService, $filter, $uibModalInstance) {
					$scope.documentResponse = {};

					$scope.popup = {
						"opened" : false
					};
					$scope.open = function() {
						$scope.popup.opened = true;
					};

					$scope.isDisable = false;
					$scope.initUserObj = function() {
						$scope.userData = {
							address : {
								country : {},
								state : {},
								city : {}
							}
						};
					}

					$scope.employmentType = [ {
						id : 1,
						name : "Salaried"
					}, {
						id : 2,
						name : "Self Employed"
					}, {
						id : 3,
						name : "Home Make/Retired"
					} ];

					$scope.selfEmploymentType = [ {
						id : 1,
						name : "Self Employed Business"
					}, {
						id : 2,
						name : "Self Employed Professional"
					} ];

					$scope.entityType = [ {
						id : 1,
						name : "Sole Proprietor"
					}, {
						id : 2,
						name : "Partnership"
					}, {
						id : 3,
						name : "Private Limited Company"
					}, {
						id : 4,
						name : "Public Limited Company"
					}, {
						id : 5,
						name : "Limited Liabiliy Partnership"
					} ];

					$scope.initUserObj();
					$scope.createNewCoApplicant = function() {
						if (!$scope.userForm.$valid) {
							$scope.userForm.$submitted = true;
							Notification
									.warning("Please fill all mandatory fields");
							return false;
						}

						if ($rootScope.isEmpty($scope.documentResponse)
								|| $rootScope
										.isEmpty($scope.documentResponse.filePath)) {
							Notification
									.warning("Please upload profile pic !!");
							return false;
						}

						$scope.isDisable = true;

						if (!$rootScope.isEmpty($scope.userData.employmentType)) {
							$scope.userData.isProfileFilled = true;
						}
						userService
								.creatCoApplicantProfile($scope.userData)
								.then(
										function(success) {
											$scope.isDisable = false;
											if (success.data.status == 200) {
												Notification
														.info("Co-Applicant Added Successfully !");
												$uibModalInstance
														.close('closed');
												$rootScope.$emit(
														'refresh-sidebar',
														success.data.data);
												$rootScope
														.$emit(
																'refresh-coapplicantList',
																success.data.data);
											} else {
												Notification
														.error(success.data.message);
											}
										},
										function(error) {
											$scope.isDisable = false;
											$rootScope
													.validateErrorResponse(error);
										});
					}

					$scope.separateName = function(data) {
						data.userTypName = $rootScope
								.getUserByType(data.userType).value;
						if ($rootScope.isEmpty(data.firstName)
								&& $rootScope.isEmpty(data.lastName)) {
							var names = data.name.split(" ");
							if (names.length == 1) {
								data.firstName = names[0].trim();
							} else if (names.length == 2) {
								data.firstName = names[0].trim();
								data.lastName = names[1].trim();
							} else if (names.length == 3) {
								data.firstName = names[0].trim();
								data.middleName = names[1].trim();
								data.lastName = names[2].trim();
							}
						}
						$rootScope.user = data;
					}

					$scope.permStates = [];
					$scope.commStates = [];
					$scope.empStates = [];
					$scope.getStates = function(countryId, type) {
						if ($rootScope.isEmpty(countryId)) {
							console
									.warn(
											"Country Id must not be null while getting States by Country Id====>",
											countryId);
							if (type == Constant.AddressType.PERMANENT) {
								$scope.permStates = [];
								$scope.permCities = [];
							} else if (type == Constant.AddressType.COMMUNICATION) {
								$scope.commStates = [];
								$scope.commCities = [];
							} else if (type == Constant.AddressType.EMPLOYMENT_ADD) {
								$scope.empStates = [];
								$scope.empCities = [];
							}
							return false;
						}
						masterService
								.states(countryId)
								.then(
										function(success) {
											if (success.data.status == 200) {
												if (type == Constant.AddressType.PERMANENT) {
													$scope.permStates = [];
													$scope.permStates = success.data.data;
												} else if (type == Constant.AddressType.COMMUNICATION) {
													$scope.commStates = [];
													$scope.commStates = success.data.data;
												} else if (type == Constant.AddressType.EMPLOYMENT_ADD) {
													$scope.empStates = [];
													$scope.empStates = success.data.data;
												}
											} else {
												Notification
														.warning(success.data.message);
											}
										},
										function(error) {
											$rootScope
													.validateErrorResponse(error);
										});
					}

					$scope.uploadAppFile = function(element) {
						$rootScope.uploadFile(element.files, null, element.id,
								$scope, true);
					}

					$scope.permCities = [];
					$scope.commCities = [];
					$scope.empCities = [];
					$scope.getCities = function(stateId, type) {
						if ($rootScope.isEmpty(stateId)) {
							console
									.warn(
											"State Id must not be null while getting States by State Id====>",
											stateId);
							if (type == Constant.AddressType.PERMANENT) {
								$scope.permCities = [];
							} else if (type == Constant.AddressType.COMMUNICATION) {
								$scope.commCities = [];
							} else if (type == Constant.AddressType.EMPLOYMENT_ADD) {
								$scope.empCities = [];
							}
							return false;
						}
						masterService
								.cities(stateId)
								.then(
										function(success) {
											if (success.data.status == 200) {
												if (type == Constant.AddressType.PERMANENT) {
													$scope.permCities = [];
													$scope.permCities = success.data.data;
												} else if (type == Constant.AddressType.COMMUNICATION) {
													$scope.commCities = [];
													$scope.commCities = success.data.data;
												} else if (type == Constant.AddressType.EMPLOYMENT_ADD) {
													$scope.empCities = [];
													$scope.empCities = success.data.data;
												}
											} else {
												Notification
														.warning(success.data.message);
											}
										},
										function(error) {
											$rootScope
													.validateErrorResponse(error);
										});
					}

					$scope.sameAddress = function(isSameUs) {
						if (isSameUs) {
							if (!$rootScope
									.isEmpty($scope.userData.communicationAdd)) {
								if (!$rootScope
										.isEmpty($scope.userData.communicationAdd.country)) {
									if ($rootScope
											.isEmpty($scope.userData.permanentAdd)) {
										$scope.userData.permanentAdd = {};
									}
									if ($rootScope
											.isEmpty($scope.userData.permanentAdd.country)) {
										$scope.userData.permanentAdd.country = {};
									}
									$scope.userData.permanentAdd.country.id = $scope.userData.communicationAdd.country.id;
									$scope
											.getStates(
													$scope.userData.communicationAdd.country.id,
													Constant.AddressType.PERMANENT);
								}
								if (!$rootScope
										.isEmpty($scope.userData.communicationAdd.state)) {
									if ($rootScope
											.isEmpty($scope.userData.permanentAdd.state)) {
										$scope.userData.permanentAdd.state = {};
									}
									$scope.userData.permanentAdd.state.id = $scope.userData.communicationAdd.state.id;
									$scope
											.getCities(
													$scope.userData.communicationAdd.state.id,
													Constant.AddressType.PERMANENT);
								}
								if ($rootScope
										.isEmpty($scope.userData.permanentAdd.city)) {
									$scope.userData.permanentAdd.city = {};
								}
								$scope.userData.permanentAdd.city.id = $scope.userData.communicationAdd.city.id;
								$scope.userData.permanentAdd.pincode = $scope.userData.communicationAdd.pincode;
								$scope.userData.permanentAdd.landMark = $scope.userData.communicationAdd.landMark;
								$scope.userData.permanentAdd.streetName = $scope.userData.communicationAdd.streetName;
								$scope.userData.permanentAdd.premisesAndBuildingName = $scope.userData.communicationAdd.premisesAndBuildingName;
							}
						} else {
							$scope.userData.permanentAdd = {};
						}
					}
				});

var ModalInstanceCtrl = function($scope, $uibModalInstance) {
	$scope.ok = function() {
		$uibModalInstance.close();
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
};

var ModalInstanceAcceptCtrl = function($scope, $uibModalInstance) {
	$scope.ok = function() {
		$uibModalInstance.close();
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
		alert("Cancel");
	};
};