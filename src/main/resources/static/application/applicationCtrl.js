angular.module("lams").controller("applicationCtrl", [ "$scope", "masterService", "$rootScope", "Notification", "applicationService", "Constant", "$filter", "$stateParams", "documentService","$state","$uibModal",
	function($scope, masterService, $rootScope, Notification, applicationService, Constant, $filter, $stateParams, documentService,$state, $uibModal) {
		$scope.applicationTypeCode = $stateParams.appCode;
		$scope.applicationTypeId = $rootScope.getAppTypeIdByCode($scope.applicationTypeCode);
		$scope.applicationId = $stateParams.appId;
		$scope.saveButtonEdit = false;
		$scope.disbursmentDate='';
		
		$scope.loanCategoryList = [
			{id : 0, name : "New"},
			{id : 1, name : "Balance Transfer"}
		];
		
		if($stateParams.mode == Constant.mode.EDIT){
			$scope.editApplicationForm = false;	
		} else if($stateParams.mode == Constant.mode.VIEW){
			$scope.editApplicationForm = true;
		} else {
			$scope.editApplicationForm = true;
		}
		
		$scope.popup ={"opened" : false};
		$scope.open = function() {
		    $scope.popup.opened = true;
		  };
		
		$scope.connections = [];
		$scope.statuses = [ Constant.Status.RESPONDED, Constant.Status.ACCEPTED, Constant.Status.REJECTED, Constant.Status.FORMAPPLIED, Constant.Status.SANCTIONED, Constant.Status.DISBURSED ];
		$scope.status = Constant.Status.RESPONDED;

		$scope.coApplicantList = [];
		$scope.getCoApplicantList = function() {
			applicationService.getCoApplicants().then(
				function(success) {
					if (success.data.status == 200) {
						$scope.coApplicantList = success.data.data;
					} else {
						Notification.error(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		};
		$scope.getCoApplicantList();
		
		$scope.getApplicationDetails = function() {
			applicationService.getLoanDetails($scope.applicationId, $scope.applicationTypeId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.applicationDetails = success.data.data;
						
						$scope.coApplicant = $scope.applicationDetails.coapplicants;
						
						//Co-Applicant
						if ($scope.coApplicant != null  && $scope.coApplicant[0].employmentType == Constant.EmploymentType.SALARIED) {
							$scope.getCoApplicantDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD, Constant.documentType.LAST_3_MONTH_SALARY_SLIP,
								Constant.documentType.LAST_6_MONTHS_BANK_ACCOUNT_STATEMENT, Constant.documentType.FORM_16_OR_APPOIMENT_LETTER,
								Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.EXISTING_LOAN_DOCUMENT, Constant.documentType.OTHER_DOCUMENT ]);
						} else if ($scope.coApplicant != null && $scope.applicationDetails.coapplicants != null && $scope.coApplicant[0].employmentType == Constant.EmploymentType.SELF_EMPLOYED) {
							$scope.getCoApplicantDocumentList([  Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD,
								Constant.documentType.INDIVIDUAL_ITR_SET_YEAR1,
								Constant.documentType.INDIVIDUAL_ITR_SET_YEAR2, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR3,
								Constant.documentType.CORPORATE_BANK_ACCOUNT_STATEMENT,
								Constant.documentType.CORPORATE_ITR_SET_YEAR1, Constant.documentType.CORPORATE_ITR_SET_YEAR2,
								Constant.documentType.CORPORATE_ITR_SET_YEAR3,
								Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.OTHER_DOCUMENT,Constant.documentType.EXISTING_LOAN_DOCUMENT ]);
						}
						else if ($scope.coApplicant != null && ($scope.applicationDetails.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED || 
								$scope.coApplicant[0].employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED)) {
							$scope.getCoApplicantDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD ]);
						}
					
						if(!$rootScope.isEmpty($scope.applicationDetails.bankAccNumber)){
							$scope.applicationDetails.bankAccNumber = parseInt($scope.applicationDetails.bankAccNumber);
						}
						
						//Aplicant
						if ($scope.applicationDetails.employmentType == Constant.EmploymentType.SALARIED) {
							$scope.getDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD, Constant.documentType.LAST_3_MONTH_SALARY_SLIP,
								Constant.documentType.LAST_6_MONTHS_BANK_ACCOUNT_STATEMENT, Constant.documentType.FORM_16_OR_APPOIMENT_LETTER,
								Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.EXISTING_LOAN_DOCUMENT, Constant.documentType.OTHER_DOCUMENT ]);
						} else if ($scope.applicationDetails.employmentType == Constant.EmploymentType.SELF_EMPLOYED) {
							$scope.getDocumentList([  Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD,
								Constant.documentType.INDIVIDUAL_ITR_SET_YEAR1,
								Constant.documentType.INDIVIDUAL_ITR_SET_YEAR2, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR3,
								Constant.documentType.CORPORATE_BANK_ACCOUNT_STATEMENT,
								Constant.documentType.CORPORATE_ITR_SET_YEAR1, Constant.documentType.CORPORATE_ITR_SET_YEAR2,
								Constant.documentType.CORPORATE_ITR_SET_YEAR3,
								Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.OTHER_DOCUMENT,Constant.documentType.EXISTING_LOAN_DOCUMENT ]);
						}
						else if ($scope.applicationDetails.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED) {
							$scope.getDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD ]);
						}
						$scope.getProcessingDocumentList([ Constant.documentType.SUBMIT_BORROWER_APPLICATION_FORM, Constant.documentType.SACTIONED_DOCUMENT, Constant.documentType.DISBURSED_DOCUMENT ]);
						
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getApplicationDetails();
		
		$scope.getSelectedCoApplicantDetails = function(selectedCoapplicant) {
			applicationService.getSelectedCoApplicantDetails(selectedCoapplicant).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.coApplicant = success.data.data;
						
						//Co-Applicant
						if ($scope.coApplicant != null  && $scope.coApplicant[0].employmentType == Constant.EmploymentType.SALARIED) {
							$scope.getCoApplicantDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD, Constant.documentType.LAST_3_MONTH_SALARY_SLIP,
								Constant.documentType.LAST_6_MONTHS_BANK_ACCOUNT_STATEMENT, Constant.documentType.FORM_16_OR_APPOIMENT_LETTER,
								Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.EXISTING_LOAN_DOCUMENT, Constant.documentType.OTHER_DOCUMENT ]);
						} else if ($scope.coApplicant != null && $scope.applicationDetails.coapplicants != null && $scope.coApplicant[0].employmentType == Constant.EmploymentType.SELF_EMPLOYED) {
							$scope.getCoApplicantDocumentList([  Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD,
								Constant.documentType.INDIVIDUAL_ITR_SET_YEAR1,
								Constant.documentType.INDIVIDUAL_ITR_SET_YEAR2, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR3,
								Constant.documentType.CORPORATE_BANK_ACCOUNT_STATEMENT,
								Constant.documentType.CORPORATE_ITR_SET_YEAR1, Constant.documentType.CORPORATE_ITR_SET_YEAR2,
								Constant.documentType.CORPORATE_ITR_SET_YEAR3,
								Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.OTHER_DOCUMENT,Constant.documentType.EXISTING_LOAN_DOCUMENT ]);
						}
						else if ($scope.coApplicant != null && ($scope.applicationDetails.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED || 
								$scope.coApplicant[0].employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED)) {
							$scope.getCoApplicantDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD ]);
						}
					
						if(!$rootScope.isEmpty($scope.applicationDetails.bankAccNumber)){
							$scope.applicationDetails.bankAccNumber = parseInt($scope.applicationDetails.bankAccNumber);
						}
						
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}

		$scope.saveLoanDetails = function(type) {
			
			if($scope.applicationDetails.isLoanDetailsLock){
				if(type == 1){
					Notification.warning("Your Application is Locked !!");
					return;
				} else if(type == 2){
					Notification.warning("Applicant is locked. Loan inquiry is submitted to lenders. You will receive notification when there is revert from lenders. Thank you.");
					return;
				}
			}
			
			var data = {};
			data.applicationTypeId = $scope.applicationTypeId;
			var uploadAll = true;
			for (var i = 0; i < $scope.documentList.length; i++) {
				if ($scope.applicationDetails.employmentType == Constant.EmploymentType.SALARIED) {
					if($scope.documentList[i].documentMstrId == Constant.documentType.PAN_CARD
							|| $scope.documentList[i].documentMstrId == Constant.documentType.AADHAR_CARD
							|| $scope.documentList[i].documentMstrId == Constant.documentType.LAST_3_MONTH_SALARY_SLIP
							|| $scope.documentList[i].documentMstrId == Constant.documentType.LAST_6_MONTHS_BANK_ACCOUNT_STATEMENT){
						if ($scope.documentList[i].documentResponseList.length == 0) {
							uploadAll = false;
						}
					}	
				} else if ($scope.applicationDetails.employmentType == Constant.EmploymentType.SELF_EMPLOYED) {
					if($scope.documentList[i].documentMstrId == Constant.documentType.PAN_CARD
							|| $scope.documentList[i].documentMstrId == Constant.documentType.AADHAR_CARD
							|| $scope.documentList[i].documentMstrId == Constant.documentType.CORPORATE_BANK_ACCOUNT_STATEMENT
							|| $scope.documentList[i].documentMstrId == Constant.documentType.INDIVIDUAL_ITR_SET_YEAR1
							|| $scope.documentList[i].documentMstrId == Constant.documentType.INDIVIDUAL_ITR_SET_YEAR2
							|| $scope.documentList[i].documentMstrId == Constant.documentType.INDIVIDUAL_ITR_SET_YEAR3){
						if ($scope.documentList[i].documentResponseList.length == 0) {
							uploadAll = false;
						}
					}
				} else if ($scope.applicationDetails.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED) {
					if($scope.documentList[i].documentMstrId == Constant.documentType.PAN_CARD
							|| $scope.documentList[i].documentMstrId == Constant.documentType.AADHAR_CARD){
						if ($scope.documentList[i].documentResponseList.length == 0) {
							uploadAll = false;
						}
					}
				}
			}
			
			if ($scope.applicationDetails.coapplicants != null && $scope.coApplicant[0].employmentType > 0){
				//coApplicantDocumentList
				for (var i = 0; i < $scope.coApplicantDocumentList.length; i++) {
					if ($scope.coApplicant[0].employmentType == Constant.EmploymentType.SALARIED) {
						if($scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.PAN_CARD
								|| $scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.AADHAR_CARD
								|| $scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.LAST_3_MONTH_SALARY_SLIP
								|| $scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.LAST_6_MONTHS_BANK_ACCOUNT_STATEMENT){
							if ($scope.coApplicantDocumentList[i].documentResponseList.length == 0) {
								uploadAll = false;
							}
						}	
					} else if ($scope.coApplicant[0].employmentType == Constant.EmploymentType.SELF_EMPLOYED) {
						if($scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.PAN_CARD
								|| $scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.AADHAR_CARD
								|| $scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.CORPORATE_BANK_ACCOUNT_STATEMENT
								|| $scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.INDIVIDUAL_ITR_SET_YEAR1
								|| $scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.INDIVIDUAL_ITR_SET_YEAR2
								|| $scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.INDIVIDUAL_ITR_SET_YEAR3){
							if ($scope.coApplicantDocumentList[i].documentResponseList.length == 0) {
								uploadAll = false;
							}
						}
					} else if ($scope.coApplicant[0].employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED) {
						if($scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.PAN_CARD
								|| $scope.coApplicantDocumentList[i].documentMstrId == Constant.documentType.AADHAR_CARD){
							if ($scope.coApplicantDocumentList[i].documentResponseList.length == 0) {
								uploadAll = false;
							}
						}
					}
				}
			}
			$scope.applicationDetails.isUploadComplete = uploadAll;
			
			if (type == 2) {
				if (!$scope.currentForm.$valid) {
					Notification.warning("Please Fill All Details Before Submit Form");
					return;
				}
				if (!uploadAll) {
					Notification.warning("Please submit all the mandatory documents to proceed with submitting your loan inquiry with multiple lenders.");
					return;
				}
				$scope.applicationDetails.isLoanDetailsComplete = true;
				$scope.applicationDetails.isLoanDetailsLock = true;
			} else {
				$scope.applicationDetails.isLoanDetailsComplete = $scope.currentForm.$valid;
			}
			data.data = JSON.stringify($scope.applicationDetails);
			applicationService.save(data).then(
				function(success) {
					if (success.data.status == 200) {
						Notification.info("Successfully updated application !!");
						if (type == 1) {
							if($stateParams.mode == Constant.mode.EDIT){
								$state.go("web.lams.application",{mode : Constant.mode.VIEW,appCode : $scope.applicationTypeCode , appId : $scope.applicationId},{reload: true});
							} else {
								$scope.editApplicationForm = !$scope.editApplicationForm;	
							}
							
						}
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.openModal = function(selected) {
			if(selected == 5 || $scope.applicationDetails.coApplicantUserId == 5){
				$scope.modalInstance = $uibModal.open({
					ariaLabelledBy : 'modal-title',
					ariaDescribedBy : 'modal-body',
					templateUrl : 'application/coApplicantProfile.html',
					controller : 'coApplicantProfileCtrl',
					controllerAs : '$ctrl',
					size : 'lg',
					resolve : {
					}
				}).result.then(function(){
					console.log("Modal is dimissed !");
				},
				function(res){
					$scope.applicationDetails.coApplicantUserId={};
				});
			}
			else{
				if(selected == null){
					$scope.coApplicant = {};
					$scope.coApplicantDocumentList = [];
				}
				else{
					$scope.getSelectedCoApplicantDetails(selected);
				}
				
			}
		}

		$scope.uploadAppFile = function(element) {
			$scope.userCoapplicantUserIdentifier = null;
			$rootScope.uploadFile(element.files, $scope.applicationId, element.id, $scope,false);
		}
		
		$scope.uploadBrCoApplicantAppFile = function(element) {
			$scope.userCoapplicantUserIdentifier = $scope.coApplicant[0].id;
			$rootScope.uploadFile(element.files, $scope.applicationId, element.id, $scope,false);
		}

		$scope.documentList = [];
		$scope.getDocumentList = function(listOfDocumentMstId) {
			if ($scope.applicationDetails.loanTypeId == Constant.LoanType.EXISTING_LOAN ||
				$scope.applicationDetails.loanTypeId == Constant.LoanType.CLOSED_LOAN) {
				return;
			}
			documentService.getDocumentList($scope.applicationId, listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.documentList = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.coApplicantDocumentList = [];
		$scope.getCoApplicantDocumentList = function(listOfDocumentMstId) {
			if ($scope.applicationDetails.loanTypeId == Constant.LoanType.EXISTING_LOAN ||
				$scope.applicationDetails.loanTypeId == Constant.LoanType.CLOSED_LOAN) {
				return;
			}
			documentService.getCoApplicantsDocumentList($scope.applicationId, $scope.coApplicant[0].id, listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.coApplicantDocumentList = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.processingDocumentList = [];
		$scope.processSubmitFormDocuments={};
		$scope.processSactionedFormDocuments={};
		$scope.processDisbursedDocumentDocuments={};
		
		$scope.getProcessingDocumentList = function(listOfDocumentMstId) {
			if ($scope.applicationDetails.loanTypeId == Constant.LoanType.EXISTING_LOAN ||
				$scope.applicationDetails.loanTypeId == Constant.LoanType.CLOSED_LOAN) {
				return;
			}
			documentService.getDocumentsListForProcessing($scope.applicationId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.processingDocumentList = success.data.data;
						$scope.processSubmitFormDocuments = success.data.data[0];
						$scope.processSactionedFormDocuments = success.data.data[1];
						$scope.processDisbursedDocumentDocuments = success.data.data[2];
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}



		$scope.inActiveDocument = function(documentMappingId, documentMapId, documentResponseList, index) {
			documentService.inActiveDocument(documentMappingId).then(
				function(success) {
					if (success.data.status == 200) {
						Notification.info("Successfully inactive documents !!");
						documentResponseList.splice(index, 1);
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}

		$scope.getConnections = function(appId, status) {
			applicationService.getConnections(appId, status).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.connections = success.data.data;
					} else {
						Notification.error(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		};

		$scope.getConnections($scope.applicationId, Constant.Status.RESPONDED);

		$scope.updateStatus = function(con, status) {
			applicationService.updateStatus(con, status).then(
				function(success) {
					if (success.data.status == 200) {
						if (success.data) {
							Notification.success("Successfully Lender Selected!");
							$scope.getConnections($scope.applicationId, Constant.Status.ACCEPTED);
						}
					} else {
						Notification.error(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		};

		$scope.curSelectedLender = {};
		$scope.setLenderInfo = function(con, status) {
			$scope.curSelectedLender.con = con;
			$scope.curSelectedLender.status = status;
			$scope.curSelectedLender.processingFees = con.processingFees;
		}
		
		$scope.autoUpdateStatus = function (){
			applicationService.disbursmentSaved($scope.applicationId, jQuery("#disbursmentDate").val()).then(
		            function(success) {
		            	if(success.data.status == 200){
		            		Notification.success("Application Status Changed!");
		            		$scope.saveButtonEdit = false;
		                }else{
		                	Notification.error(success.data.message);
		                }
		            }, function(error) {
		            	$rootScope.validateErrorResponse(error);
		     });
		};
		
		$rootScope.$on('refresh-coapplicantList',function(event,data){
			$scope.coApplicantList = data;
	    });

	} ]);

angular.module("lams").controller("coApplicantProfileCtrl", function($scope, $http, $rootScope,Constant,userService,Notification,masterService,$filter, $uibModalInstance) {
	$scope.documentResponse = {};
	
	$scope.popup ={"opened" : false};
	$scope.open = function() {
	    $scope.popup.opened = true;
	  };
	
	$scope.isDisable = false;
	$scope.initUserObj = function(){
		$scope.userData = { address : {
			country : {},
			state : {},
			city : {}
		}};		
	}
	
	$scope.employmentType = [
		{id : 1, name : "Salaried"},
		{id : 2, name : "Self Employed"},
		{id : 3, name : "Home Make/Retired"}
	];
	
	$scope.selfEmploymentType = [
		{id : 1, name : "Self Employed Business"},
		{id : 2, name : "Self Employed Professional"}
	];
	
	$scope.entityType = [
		{id : 1, name : "Sole Proprietor"},
		{id : 2, name : "Partnership"},
		{id : 3, name : "Private Limited Company"},
		{id : 4, name : "Public Limited Company"},
		{id : 5, name : "Limited Liabiliy Partnership"}
	];
	
	$scope.initUserObj();
	$scope.createNewCoApplicant = function(){
		if (!$scope.userForm.$valid) {
			$scope.userForm.$submitted = true;
			Notification.warning("Please fill all mandatory fields");
			return false;
		}
		
		if($rootScope.isEmpty($scope.documentResponse) || $rootScope.isEmpty($scope.documentResponse.filePath)){
			Notification.warning("Please upload profile pic !!");
			return false;
		}
		
		$scope.isDisable = true;
		
		if(!$rootScope.isEmpty($scope.userData.employmentType)){
			$scope.userData.isProfileFilled = true;	
		}
		userService.creatCoApplicantProfile($scope.userData).then(
	            function(success) {
	            	$scope.isDisable = false;
	            	if(success.data.status == 200){
	            		Notification.info("Co-Applicant Added Successfully !");
	            		$uibModalInstance.close('closed');
	            		$rootScope.$emit('refresh-sidebar',success.data.data);
	            		$rootScope.$emit('refresh-coapplicantList',success.data.data);
	                }else{
	                	Notification.error(success.data.message);
	                }
	            }, function(error) {
	            	$scope.isDisable = false;
	            	$rootScope.validateErrorResponse(error);
	     });		
	}
	
	$scope.separateName = function(data){
		data.userTypName = $rootScope.getUserByType(data.userType).value;
		if($rootScope.isEmpty(data.firstName) && $rootScope.isEmpty(data.lastName)){
			var names = data.name.split(" ");
    		if(names.length == 1){
    			data.firstName = names[0].trim();	
    		}else if(names.length == 2){
    			data.firstName = names[0].trim();
    			data.lastName = names[1].trim();
    		}else if(names.length == 3){
    			data.firstName = names[0].trim();
    			data.middleName = names[1].trim();
    			data.lastName = names[2].trim();
    		}
		}
		$rootScope.user = data;
	}

	$scope.permStates = [];
	$scope.commStates = [];
	$scope.empStates = [];
	$scope.getStates = function(countryId,type){
		if($rootScope.isEmpty(countryId)){
			console.warn("Country Id must not be null while getting States by Country Id====>",countryId);
			if(type == Constant.AddressType.PERMANENT){
    			$scope.permStates = [];	
    			$scope.permCities = [];
    		} else if(type == Constant.AddressType.COMMUNICATION){
    			$scope.commStates = [];	
    			$scope.commCities = [];
    		} else if(type == Constant.AddressType.EMPLOYMENT_ADD){
    			$scope.empStates = [];	
    			$scope.empCities = [];
    		}
			return false;
		}
		masterService.states(countryId).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		if(type == Constant.AddressType.PERMANENT){
	            			$scope.permStates = [];
	            			$scope.permStates = success.data.data;	
	            		} else if(type == Constant.AddressType.COMMUNICATION){
	            			$scope.commStates = [];
	            			$scope.commStates = success.data.data;	
	            		}  else if(type == Constant.AddressType.EMPLOYMENT_ADD){
	            			$scope.empStates = [];
	            			$scope.empStates = success.data.data;
	            		} 
	            	} else{
	            		Notification.warning(success.data.message);
	            	}
	            }, function(error) {
	            	$rootScope.validateErrorResponse(error);
	     });		
	}
	
	$scope.uploadAppFile = function(element) {
		$rootScope.uploadFile(element.files, null, element.id, $scope,true);
	}
	
	$scope.permCities = [];
	$scope.commCities = [];
	$scope.empCities = [];
	$scope.getCities = function(stateId,type){
		if($rootScope.isEmpty(stateId)){
			console.warn("State Id must not be null while getting States by State Id====>",stateId);
			if(type == Constant.AddressType.PERMANENT){
    			$scope.permCities = [];	
    		} else if(type == Constant.AddressType.COMMUNICATION){
    			$scope.commCities = [];	
    		} else if(type == Constant.AddressType.EMPLOYMENT_ADD){
    			$scope.empCities = [];
    		}
			return false;
		}
		masterService.cities(stateId).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		if(type == Constant.AddressType.PERMANENT){
	            			$scope.permCities = [];
	            			$scope.permCities = success.data.data;	
	            		} else if(type == Constant.AddressType.COMMUNICATION){
	            			$scope.commCities = [];
	            			$scope.commCities = success.data.data;	
	            		}  else if(type == Constant.AddressType.EMPLOYMENT_ADD){
	            			$scope.empCities = [];
	            			$scope.empCities = success.data.data;
	            		}
	            	}else{
	            		Notification.warning(success.data.message);
	            	}
	            }, function(error) {
	            	$rootScope.validateErrorResponse(error);
	     });		
	}
	
	$scope.sameAddress = function(isSameUs){
		if(isSameUs){
			if(!$rootScope.isEmpty($scope.userData.communicationAdd)){
    			if(!$rootScope.isEmpty($scope.userData.communicationAdd.country)){
    				if($rootScope.isEmpty($scope.userData.permanentAdd)){
    					$scope.userData.permanentAdd = {};
    				}
    				if($rootScope.isEmpty($scope.userData.permanentAdd.country)){
						$scope.userData.permanentAdd.country = {};	
					}
    				$scope.userData.permanentAdd.country.id = $scope.userData.communicationAdd.country.id;
    				$scope.getStates($scope.userData.communicationAdd.country.id,Constant.AddressType.PERMANENT);
    			}
    			if(!$rootScope.isEmpty($scope.userData.communicationAdd.state)){
    				if($rootScope.isEmpty($scope.userData.permanentAdd.state)){
    					$scope.userData.permanentAdd.state = {};	
					}
    				$scope.userData.permanentAdd.state.id = $scope.userData.communicationAdd.state.id;
    				$scope.getCities($scope.userData.communicationAdd.state.id,Constant.AddressType.PERMANENT);	
    			}
    			if($rootScope.isEmpty($scope.userData.permanentAdd.city)){
    				$scope.userData.permanentAdd.city = {};	
				}
    			$scope.userData.permanentAdd.city.id = $scope.userData.communicationAdd.city.id;
    			$scope.userData.permanentAdd.pincode = $scope.userData.communicationAdd.pincode;
    			$scope.userData.permanentAdd.landMark = $scope.userData.communicationAdd.landMark;
    			$scope.userData.permanentAdd.streetName = $scope.userData.communicationAdd.streetName;
    			$scope.userData.permanentAdd.premisesAndBuildingName = $scope.userData.communicationAdd.premisesAndBuildingName;
    		}
		} else {
			$scope.userData.permanentAdd = {};	
		}
	}
});