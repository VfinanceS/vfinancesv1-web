angular.module("lams").controller( "cpPayoutDashboardCtrl",[ "$scope", "$rootScope", "Constant","payoutService",
	function($scope, $rootScope, Constant, payoutService) {
		$scope.allPayOuts = [];
		$scope.getAllPayOuts = function() {
			payoutService.getPaymentDetails().then(
				function(success) {
					if (success.data.status == 200) {
						$scope.allPayOuts = success.data.data;
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				},
				function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getAllPayOuts();
} ]);