angular.module("lams").controller("brLoanProfileCtrl", [ "$scope", "$http", "$rootScope", "Constant", "userService", "Notification", "masterService", "$filter", "$stateParams", "applicationService","documentService",
	function($scope, $http, $rootScope, Constant, userService, Notification, masterService, $filter, $stateParams, applicationService,documentService) {

		$scope.currentUser = {};
		$scope.brId = $stateParams.brId;
		$scope.processSubmitForm='';
		$scope.processSactionedForm='';
		$scope.processDisbursedDocument='';
		
		$scope.processSubmitFormDocuments={};
		$scope.processSactionedFormDocuments={};
		$scope.processDisbursedDocumentDocuments={};

		$scope.loanCategoryList = [
   			{id : 0, name : "New"},
   			{id : 1, name : "Balance Transfer"}
   		];
		
		var appId = $stateParams.appId;
		var appTypeId = $stateParams.appTypeId;

		function getUserDetailsById(brId) {
			userService.getUserDetailsById(brId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.userData = success.data.data;
						
                        $scope.applicationList = $scope.userData.applications;
                        $scope.totalExistingLoanAmount = 0;
                        $scope.totalExistingLoanEMI = 0;
                        $scope.existingLoanList = $filter('filter')($scope.applicationList,{loanTypeId : Constant.LoanType.EXISTING_LOAN});
						for(var i = 0; i < $scope.existingLoanList.length; i++){
							if(!$rootScope.isEmpty($scope.existingLoanList[i].loanAmount)){
								$scope.totalExistingLoanAmount = $scope.totalExistingLoanAmount + parseFloat($scope.existingLoanList[i].loanAmount);
								$scope.totalExistingLoanEMI = $scope.totalExistingLoanEMI + parseFloat($scope.existingLoanList[i].emi);
							}
						}
                                                
						$scope.userData.applications = $filter('filter')($scope.userData.applications, {
							id : appId
						});
						
						//Co-Applicant Documents
						if(typeof($scope.userData.applications[0].userCoApplicantBO) != 'undefined' ){
							$scope.coApplicant = $scope.userData.applications[0].userCoApplicantBO;
							
							if ($scope.coApplicant != null && $scope.coApplicant.employmentType == Constant.EmploymentType.SALARIED) {
								$scope.getCoApplicantDocumentList([ Constant.documentType.PHOTO_GRAPH, Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD, Constant.documentType.LAST_3_MONTH_SALARY_SLIP,
									Constant.documentType.LAST_6_MONTHS_BANK_ACCOUNT_STATEMENT, Constant.documentType.FORM_16_OR_APPOIMENT_LETTER,
									Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.EXISTING_LOAN_DOCUMENT, Constant.documentType.OTHER_DOCUMENT ]);
							} else if ($scope.coApplicant != null && $scope.coApplicant.employmentType == Constant.EmploymentType.SELF_EMPLOYED) {
								$scope.getCoApplicantDocumentList([ Constant.documentType.PHOTO_GRAPH, Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD,
									Constant.documentType.CORPORATE_ITR_SET_YEAR1, Constant.documentType.CORPORATE_ITR_SET_YEAR2,
									Constant.documentType.CORPORATE_ITR_SET_YEAR3,
									Constant.documentType.CORPORATE_BANK_ACCOUNT_STATEMENT, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR1,
									Constant.documentType.INDIVIDUAL_ITR_SET_YEAR2, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR3,
									Constant.documentType.INDIVIDUAL_BANK_ACCOUNT_STATEMENT ]);
							} else if ($scope.coApplicant != null && ($scope.userData.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED || 
									($scope.coApplicant != null && $scope.coApplicant.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED))) {
								$scope.getCoApplicantDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD ]);
							}
						}
						
						//Applicant documents
						if ($scope.userData.employmentType == Constant.EmploymentType.SALARIED) {
							$scope.getDocumentList([ Constant.documentType.PHOTO_GRAPH, Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD, Constant.documentType.LAST_3_MONTH_SALARY_SLIP,
								Constant.documentType.LAST_6_MONTHS_BANK_ACCOUNT_STATEMENT, Constant.documentType.FORM_16_OR_APPOIMENT_LETTER,
								Constant.documentType.INVESTMENT_PROOFS, Constant.documentType.EXISTING_LOAN_DOCUMENT, Constant.documentType.OTHER_DOCUMENT ]);
						} else if ($scope.userData.employmentType == Constant.EmploymentType.SELF_EMPLOYED) {
							$scope.getDocumentList([ Constant.documentType.PHOTO_GRAPH, Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD,
								Constant.documentType.CORPORATE_ITR_SET_YEAR1, Constant.documentType.CORPORATE_ITR_SET_YEAR2,
								Constant.documentType.CORPORATE_ITR_SET_YEAR3,
								Constant.documentType.CORPORATE_BANK_ACCOUNT_STATEMENT, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR1,
								Constant.documentType.INDIVIDUAL_ITR_SET_YEAR2, Constant.documentType.INDIVIDUAL_ITR_SET_YEAR3,
								Constant.documentType.INDIVIDUAL_BANK_ACCOUNT_STATEMENT ]);
						} else if ($scope.userData.employmentType == Constant.EmploymentType.HOME_MAKER_OR_RETIRED) {
							$scope.getDocumentList([ Constant.documentType.PAN_CARD, Constant.documentType.AADHAR_CARD ]);
						}
						
						//Processing Documents
						$scope.getDocumentsListForProcessing(appId);
						
						userService.getLoggedInUserDetail().then(
								function(success) {
									if (success.data.status == 200) {
										$scope.currentUser = success.data.data;
									} else {
										Notification.error(success.data.message);
									}
								}, function(error) {
									$rootScope.validateErrorResponse(error);
								});
					} else {
						Notification.error(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		getUserDetailsById($scope.brId);

    $scope.documentList = [];
		$scope.getDocumentList = function(listOfDocumentMstId) {
			documentService.getDocumentList(appId, listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.documentList = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		};
		
		$scope.brProfileImage = function() {
			documentService.getSelectedUserDocument($scope.brId, Constant.documentType.PHOTO_GRAPH).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.brProfileImageData = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		};
		$scope.brProfileImage();
		
		$scope.coApplicantDocumentList = [];
		$scope.getCoApplicantDocumentList = function(listOfDocumentMstId) {
			
			documentService.getCoApplicantsDocumentList(appId, $scope.coApplicant.id, listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.coApplicantDocumentList = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		
		$scope.setApplicationData = function(app) {
			$scope.respond = {
				application : {},
				applicationMappingBO : {
					id : appTypeId
				}
			};
			$scope.respond.application = app;
			$scope.respond.canEdit = true;
			$scope.selectedAppId='';

			$scope.processSubmitFormDocuments={};
			$scope.processSactionedFormDocuments={};
			$scope.processDisbursedDocumentDocuments={};
		};

		$scope.submitApproval = function (){
		if (!$scope.responseForm.$valid) {
			Notification.warning("Please fill all mandatory fields");
			return false;
		}
		
		applicationService.saveApprovalRequest($scope.respond).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		Notification.success("Request has been sent");
//	            		$scope.userData = success.data.data;
	            		getUserDetailsById($scope.brId);
	                }else{
	                	Notification.error(success.data.message);
	                }
	            }, function(error) {
	            	$rootScope.validateErrorResponse(error);
	     });
	};
	
	$scope.updateStatus = function (){
		//$scope.respond.id=appId;
		applicationService.updateStatus(appId).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		Notification.success("Application Status Changed!");
//	            		$scope.userData = success.data.data;
	            		getUserDetailsById($scope.brId);
	            		$scope.saveButtonEdit = false;
	                }else{
	                	Notification.error(success.data.message);
	                }
	            }, function(error) {
	            	$rootScope.validateErrorResponse(error);
	     });
	};
	
	$scope.setNotInterestedStatus = function (app){
		
		$scope.respond = {
				application : {},
				applicationMappingBO : {
					id : appTypeId
				}
			};
			$scope.respond.application = app;
		
		applicationService.setNotInterestedStatus($scope.respond).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		Notification.success("Request has been sent");
//	            		$scope.userData = success.data.data;
	            		getUserDetailsById($scope.brId);
	                }else{
	                	Notification.error(success.data.message);
	                }
	            }, function(error) {
	            	$rootScope.validateErrorResponse(error);
	     });
	};
	
		$scope.getRespondedApplications = function(application) {

			applicationService.getRespondedApplications($scope.brId, application.id).then(
				function(success) {
					if (success.data.status == 200) {
						application.respondedApplications = success.data.data;
					} else {
						Notification.error(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		};

		$scope.appData = {};
		$scope.viewRespondedData = function(respondedData, currentStatus) {
			console.log("selectedAppId : "+appId);
			$scope.respond = respondedData[0];
			$scope.formSubmissionFileEdit = true;
			$scope.sactionedFileEdit = true;
			$scope.disbursmentFileEdit = true;
			$scope.saveButtonEdit = false;
			$scope.formSubmitView = false;
			
			if(currentStatus == 'ACCEPTED'){
				$scope.formSubmitView = true;
			}
			
			userService.getLoggedInUserDetail().then(
					function(success) {
						if (success.data.status == 200) {
							$scope.currentUser = success.data.data;
						} else {
							Notification.error(success.data.message);
						}
					}, function(error) {
						$rootScope.validateErrorResponse(error);
					});
			
			$scope.getDocumentsListForProcessing(appId);
		};
		
		$scope.getDocumentsListForProcessing = function(appId) {
			applicationService.getDocumentsListForProcessing(appId).then(
					function(success) {
						if (success.data.status == 200) {
							
							$scope.processSubmitFormDocuments=success.data.data[0];
							$scope.processSactionedFormDocuments=success.data.data[1];
							$scope.processDisbursedDocumentDocuments=success.data.data[2];
							
							if($scope.processSubmitFormDocuments.documentResponseList.length > 0){
								$scope.formSubmissionFileEdit = false;
							}
							
							if($scope.processSactionedFormDocuments.documentResponseList.length > 0){
								$scope.sactionedFileEdit = true;
							}
							
						} else {
							Notification.error(success.data.message);
						}
					}, function(error) {
						$rootScope.validateErrorResponse(error);
					});
		}
		
		$scope.uploadAppFile = function(element) {
			$rootScope.uploadFile(element.files, appId, element.id, $scope,false);
		}
		
		$scope.autoUpdateStatus = function (){
			applicationService.autoUpdateStatus(appId).then(
		            function(success) {
		            	if(success.data.status == 200){
		            		Notification.success("Application Status Changed!");
		            		getUserDetailsById($scope.brId);
		            		$scope.saveButtonEdit = false;
		                }else{
		                	Notification.error(success.data.message);
		                }
		            }, function(error) {
		            	$rootScope.validateErrorResponse(error);
		     });
		};
		 
		$scope.getPDReportingDocumentList = function(listOfDocumentMstId) {
			documentService.getPDReportingDocumentsList(appId, listOfDocumentMstId, "PDI_REPORT").then(
				function(success) {
					if (success.data.status == 200) {
						$scope.pdReportDocuments = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getPDReportingDocumentList([ Constant.documentType.PDI_REPORT_DOCUMENT ]);
		
		$scope.getFCIReportingDocumentList = function(listOfDocumentMstId) {
			documentService.getPDReportingDocumentsList(appId, listOfDocumentMstId, "FCI_REPORT").then(
				function(success) {
					if (success.data.status == 200) {
						$scope.fciReportDocuments = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getFCIReportingDocumentList([ Constant.documentType.FCI_REPORT_DOCUMENT ]);

	} ]);