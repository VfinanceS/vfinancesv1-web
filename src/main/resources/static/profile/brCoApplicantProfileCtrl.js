angular.module("lams").controller("brCoApplicantProfileCtrl",["$scope", "$http","$rootScope","Constant","userService","Notification","masterService","$filter","$stateParams", "documentService","$state",
		function($scope, $http, $rootScope, Constant,userService,Notification,masterService,$filter,$stateParams,documentService,$state) {
	
	$scope.coApplicantUserId = $stateParams.id;
	$scope.coApplicantParentUserId = $stateParams.parentUserId;
	
	$scope.coApplicantDocumentResponse = {};
	
	$scope.popup ={"opened" : false};
	$scope.open = function() {
	    $scope.popup.opened = true;
	  };
	
	$scope.isDisable = false;
	$scope.initUserObj = function(){
		$scope.userCoApplicantData = { address : {
			country : {},
			state : {},
			city : {}
		}};		
	}
	
	$scope.employmentType = [
		{id : 1, name : "Salaried"},
		{id : 2, name : "Self Employed"},
		{id : 3, name : "Home Make/Retired"}
	];
	
	$scope.selfEmploymentType = [
		{id : 1, name : "Self Employed Business"},
		{id : 2, name : "Self Employed Professional"}
	];
	
	$scope.entityType = [
		{id : 1, name : "Sole Proprietor"},
		{id : 2, name : "Partnership"},
		{id : 3, name : "Private Limited Company"},
		{id : 4, name : "Public Limited Company"},
		{id : 5, name : "Limited Liabiliy Partnership"}
		
	];
	
	$scope.initUserObj();
	
	$scope.updateUserCoApplicantDetail = function(){
		if (!$scope.userForm.$valid) {
			$scope.userForm.$submitted = true;
			Notification.warning("Please fill all mandatory fields");
			return false;
		}
		
		if($rootScope.isEmpty($scope.coApplicantDocumentResponse) || $rootScope.isEmpty($scope.coApplicantDocumentResponse.filePath)){
			Notification.warning("Please upload profile pic !!");
			return false;
		}
		
		$scope.isDisable = true;
		
		if(!$rootScope.isEmpty($scope.userCoApplicantData.employmentType)){
			$scope.userCoApplicantData.isProfileFilled = true;	
		}
		
		userService.updateUserCoApplicantDetail($scope.userCoApplicantData, $scope.userCoApplicantData.id).then(
	            function(success) {
	            	$scope.isDisable = false;
	            	if(success.data.status == 200){
	            		Notification.info("Successfully Updated !");
	            		$rootScope.user = success.data.data;
	                }else{
	                	Notification.error(success.data.message);
	                }
	            }, function(error) {
	            	$scope.isDisable = false;
	            	$rootScope.validateErrorResponse(error);
	     });		
	}

	$scope.getBrCoProfile = function(){
		userService.getCoapplicantProfileDetails($scope.coApplicantUserId, $scope.coApplicantParentUserId).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		$scope.userCoApplicantData = success.data.data;
	            		$scope.getSelectedUserDocument(9,$scope.coApplicantUserId, $scope);
	            		//$scope.separateName($scope.userCoApplicantData);
	            		if(!$rootScope.isEmpty($scope.userCoApplicantData.birthDate)){
	            			$scope.userCoApplicantData.birthDate = new Date($scope.userCoApplicantData.birthDate);
	            		}
	            		
	            		if(!$rootScope.isEmpty($scope.userCoApplicantData.permanentAdd)){
	            			if(!$rootScope.isEmpty($scope.userCoApplicantData.permanentAdd.country)){
	            				$scope.getStates($scope.userCoApplicantData.permanentAdd.country.id,Constant.AddressType.PERMANENT);	
	            			}
	            			if(!$rootScope.isEmpty($scope.userCoApplicantData.permanentAdd.state)){
	            				$scope.getCities($scope.userCoApplicantData.permanentAdd.state.id,Constant.AddressType.PERMANENT);	
	            			}
	            		}
	            		if(!$rootScope.isEmpty($scope.userCoApplicantData.communicationAdd)){
	            			if(!$rootScope.isEmpty($scope.userCoApplicantData.communicationAdd.country)){
	            				$scope.getStates($scope.userCoApplicantData.communicationAdd.country.id,Constant.AddressType.COMMUNICATION);	
	            			}
	            			if(!$rootScope.isEmpty($scope.userCoApplicantData.communicationAdd.state)){
	            				$scope.getCities($scope.userCoApplicantData.communicationAdd.state.id,Constant.AddressType.COMMUNICATION);	
	            			}
	            		}
	            		
	            		if(!$rootScope.isEmpty($scope.userCoApplicantData.employmentAddress)){
	            			if(!$rootScope.isEmpty($scope.userCoApplicantData.employmentAddress.country)){
	            				$scope.getStates($scope.userCoApplicantData.employmentAddress.country.id,Constant.AddressType.EMPLOYMENT_ADD);	
	            			}
	            			if(!$rootScope.isEmpty($scope.userCoApplicantData.employmentAddress.state)){
	            				$scope.getCities($scope.userCoApplicantData.employmentAddress.state.id,Constant.AddressType.EMPLOYMENT_ADD);	
	            			}
	            		}
	                }else{
	                	Notification.error(success.data.message);
	                }
	            }, function(error) {
	            	console.log(error);
	            	$rootScope.validateErrorResponse(error);
	            
	     });		
	}
	$scope.getBrCoProfile();
	
	$scope.getSelectedUserDocument = function(documentId,userId,scope) {
		documentService.getSelectedUserDocuments(documentId,userId).then(
			function(success) {
				if (success.data.status == 200) {
					$scope.coApplicantDocumentResponse = success.data.data;	
				} else {
					Notification.warning(success.data.message);
				}
			}, function(error) {
				$rootScope.validateErrorResponse(error);
			});
	}
	
	$scope.permStates = [];
	$scope.commStates = [];
	$scope.empStates = [];
	$scope.getStates = function(countryId,type){
		if($rootScope.isEmpty(countryId)){
			console.warn("Country Id must not be null while getting States by Country Id====>",countryId);
			if(type == Constant.AddressType.PERMANENT){
    			$scope.permStates = [];	
    			$scope.permCities = [];
    		} else if(type == Constant.AddressType.COMMUNICATION){
    			$scope.commStates = [];	
    			$scope.commCities = [];
    		} else if(type == Constant.AddressType.EMPLOYMENT_ADD){
    			$scope.empStates = [];	
    			$scope.empCities = [];
    		}
			return false;
		}
		masterService.states(countryId).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		if(type == Constant.AddressType.PERMANENT){
	            			$scope.permStates = [];
	            			$scope.permStates = success.data.data;	
	            		} else if(type == Constant.AddressType.COMMUNICATION){
	            			$scope.commStates = [];
	            			$scope.commStates = success.data.data;	
	            		}  else if(type == Constant.AddressType.EMPLOYMENT_ADD){
	            			$scope.empStates = [];
	            			$scope.empStates = success.data.data;
	            		} 
	            	} else{
	            		Notification.warning(success.data.message);
	            	}
	            }, function(error) {
	            	$rootScope.validateErrorResponse(error);
	     });		
	}
	
	$scope.uploadAppFile = function(element) {
		$scope.userCoapplicantUserIdentifier=$scope.userCoApplicantData.id;
		$rootScope.uploadFile(element.files, null, element.id, $scope,true);
	}
	
	$scope.permCities = [];
	$scope.commCities = [];
	$scope.empCities = [];
	$scope.getCities = function(stateId,type){
		if($rootScope.isEmpty(stateId)){
			console.warn("State Id must not be null while getting States by State Id====>",stateId);
			if(type == Constant.AddressType.PERMANENT){
    			$scope.permCities = [];	
    		} else if(type == Constant.AddressType.COMMUNICATION){
    			$scope.commCities = [];	
    		} else if(type == Constant.AddressType.EMPLOYMENT_ADD){
    			$scope.empCities = [];
    		}
			return false;
		}
		masterService.cities(stateId).then(
	            function(success) {
	            	if(success.data.status == 200){
	            		if(type == Constant.AddressType.PERMANENT){
	            			$scope.permCities = [];
	            			$scope.permCities = success.data.data;	
	            		} else if(type == Constant.AddressType.COMMUNICATION){
	            			$scope.commCities = [];
	            			$scope.commCities = success.data.data;	
	            		}  else if(type == Constant.AddressType.EMPLOYMENT_ADD){
	            			$scope.empCities = [];
	            			$scope.empCities = success.data.data;
	            		}
	            	}else{
	            		Notification.warning(success.data.message);
	            	}
	            }, function(error) {
	            	$rootScope.validateErrorResponse(error);
	     });		
	}
	
	$scope.sameAddress = function(isSameUs){
		if(isSameUs){
			if(!$rootScope.isEmpty($scope.userCoApplicantData.communicationAdd)){
    			if(!$rootScope.isEmpty($scope.userCoApplicantData.communicationAdd.country)){
    				if($rootScope.isEmpty($scope.userCoApplicantData.permanentAdd)){
    					$scope.userCoApplicantData.permanentAdd = {};
    				}
    				if($rootScope.isEmpty($scope.userCoApplicantData.permanentAdd.country)){
						$scope.userCoApplicantData.permanentAdd.country = {};	
					}
    				$scope.userCoApplicantData.permanentAdd.country.id = $scope.userCoApplicantData.communicationAdd.country.id;
    				$scope.getStates($scope.userCoApplicantData.communicationAdd.country.id,Constant.AddressType.PERMANENT);
    			}
    			if(!$rootScope.isEmpty($scope.userCoApplicantData.communicationAdd.state)){
    				if($rootScope.isEmpty($scope.userCoApplicantData.permanentAdd.state)){
    					$scope.userCoApplicantData.permanentAdd.state = {};	
					}
    				$scope.userCoApplicantData.permanentAdd.state.id = $scope.userCoApplicantData.communicationAdd.state.id;
    				$scope.getCities($scope.userCoApplicantData.communicationAdd.state.id,Constant.AddressType.PERMANENT);	
    			}
    			if($rootScope.isEmpty($scope.userCoApplicantData.permanentAdd.city)){
    				$scope.userCoApplicantData.permanentAdd.city = {};	
				}
    			$scope.userCoApplicantData.permanentAdd.city.id = $scope.userCoApplicantData.communicationAdd.city.id;
    			$scope.userCoApplicantData.permanentAdd.pincode = $scope.userCoApplicantData.communicationAdd.pincode;
    			$scope.userCoApplicantData.permanentAdd.landMark = $scope.userCoApplicantData.communicationAdd.landMark;
    			$scope.userCoApplicantData.permanentAdd.streetName = $scope.userCoApplicantData.communicationAdd.streetName;
    			$scope.userCoApplicantData.permanentAdd.premisesAndBuildingName = $scope.userCoApplicantData.communicationAdd.premisesAndBuildingName;
    		}
		} else {
			$scope.userCoApplicantData.permanentAdd = {};	
		}
	}
}]);
