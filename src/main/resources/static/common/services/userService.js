app.service("userService", [ 'httpService', 'URLS', "$rootScope", "$http",
	function(httpService, URLS, $rootScope, $http) {

		this.pingRequest = function() {
			return httpService.get(URLS.user + "/ping");
		};

		this.register = function(data) {
			return httpService.post(URLS.user + '/registration', data);
		};

		this.login = function(data) {
			return httpService.post(URLS.user + '/login', data);
		};

		this.logout = function() {
			return httpService.get(URLS.user + '/logout');
		};

		this.verifyOTP = function(data, otpType) {
			return httpService.post(URLS.user + '/verify_otp/' + otpType, data);
		};

		this.resendOTP = function(data, otpType, templateName) {
			return httpService.post(URLS.user + '/resend_otp/' + otpType + "/" + templateName, data);
		};

		this.changePassword = function(data) {
			return httpService.post(URLS.user + '/change_password', data);
		};

		this.getLoggedInUserDetail = function() {
			return httpService.get(URLS.user + '/get_user_details');
		};

		this.getUserDetailsById = function(userId) {
			return httpService.get(URLS.user + '/get_user_details_by_id/' + userId);
		};

		this.updateUserDetail = function(userObj) {
			return httpService.post(URLS.user + '/update_user_details', userObj);
		};

		this.verifyEmail = function(link) {
			return httpService.get(URLS.user + "/verify_email/" + link);
		};

		this.sendForgotPasswordLink = function(data) {
			return httpService.post(URLS.user + "/send_link", data);
		};

		this.resetPassword = function(data, link) {
			return httpService.post(URLS.user + "/reset_password/" + link, data);
		};
		this.saveOrUpdateBorrowerByCP = function(userObj) {
			return httpService.post(URLS.user + '/save_cp_borrower',userObj);
		};
		
		this.saveOrUpdateBorrowerByCPLatest = function(data) {
			return httpService.post(URLS.user + '/confirmPreApplicationByBorrower', data);
		};
		
		this.getCpUsers = function(userType) {
			return httpService.get(URLS.user + '/get_cp_users/' + userType);
		};
		
		this.creatCoApplicantProfile = function(userObj) {
			return httpService.post(URLS.user + '/createCoApplicantForUser',userObj);
		};
		
		this.getCoapplicantProfileDetails = function(coApplicantId, coApplicantParentUserId ) {
			return httpService.get(URLS.user + "/get_coApplicantuser_details/" + coApplicantId + "/" + coApplicantParentUserId);
		};
		
		this.updateUserCoApplicantDetail = function(userObj, coApplicantUserId) {
			return httpService.post(URLS.user + '/update_user_coapplicant_details/' + coApplicantUserId, userObj );
		};
		
		this.getAllPayouts = function() {
			return httpService.get(URLS.user + "/payout/getAllPayouts");
		};
		 
		this.getPDUserPermissionMatrixDetailsById = function() {
			return httpService.get(URLS.user + "/getPdUserPermissionMatrix");
		};
		
		this.submitBorrowerAnswers = function(data) {
			return httpService.post(URLS.user + "/borrower_answers", data);
		}; 
		
		this.getQuestionsAnswers = function(applId) {
			return httpService.get(URLS.user + "/getAllQuestionAnsByAgencyType/" + applId);
		};
		
		this.getAgencyUsersConfigByUser = function() {
			return httpService.get(URLS.user + "/getAgencyUsersConfigByUser");
		};
		
	} ]);