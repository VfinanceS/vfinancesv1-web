app.service("applicationService", [ 'httpService', 'URLS', "$rootScope","$http",
		function(httpService, URLS, $rootScope, $http) {
	
	this.getAll = function() {
		return httpService.get(URLS.user + "/application/getAll");
	};
	
	this.get = function(id) {
		return httpService.get(URLS.user + "/application/get/"+id);
	};
	
	this.getRespondedApplications = function(brId, appId) {
		return httpService.get(URLS.user + "/application/get_responded_application/" +brId + "/" + appId);
	};
	
	this.getLoanDetails = function(id,appTypeId) {
		return httpService.get(URLS.user + "/application/getLoanDetails/" + id + "/" + appTypeId);
	};
	
	this.getSelectedCoApplicantDetails = function(coApplicantId) {
		return httpService.get(URLS.user + "/application/getCoApplicantDetails/" + coApplicantId );
	};
	
	this.save = function(data) {
		return httpService.post(URLS.user + "/application/save",data);
	};
	
	this.inactive = function(applicationId) {
		return httpService.get(URLS.user + "/application/delete/" + applicationId);
	};
	
	this.getBorrowerForLender = function() {
		return httpService.get(URLS.user + "/application/get_borrowers_for_lender");
	};
	
	this.getBorrowerForLenderByApplicationId = function(appId,status) {
		return httpService.get(URLS.user + "/application/get_borrowers_for_lender_app_id/" + appId + "/" + status);
	};
	 
	this.getBorrowerForPDUserByApplicationId = function(appId,status) {
		return httpService.get(URLS.user + "/application/get_borrowers_for_pduser_app_id/" + appId + "/" + status);
	};
	
	this.getConnections = function(appId,status) {
		return httpService.get(URLS.user + "/application/get_connections/" + appId + "/" + status);
	};
	
	this.saveApprovalRequest = function(data) {
		return httpService.post(URLS.user + "/application/save_approval_request",data);
	};
	
	this.setNotInterestedStatus = function(data) {
		return httpService.post(URLS.user + "/application/save_not_interested_status",data);
	};
	
	this.updateStatus = function(data,status) {
		return httpService.post(URLS.user + "/application/update_status/" + status, data);
	};

//	this.getApplicationDetailsByIdAndUserId = function(appId, userId) {
//		return httpService.get(URLS.user + "/application/get_application_details_for_lender/" + appId + "/"+ userId);
//	};
	
	this.saveApprovalRequest = function(data) {
		return httpService.post(URLS.user + "/application/save_approval_request",data);
	};
	
	this.getCoApplicants = function() {
		return httpService.get(URLS.user + "/application/get_Co_Applicants");
	};
	
	this.getDocumentsListForProcessing = function(applId) {
		return httpService.get(URLS.user + "/getDocumentsListForProcessing/"+applId);
	};

	this.getSideBarMenus = function() {
		applicationService.getCoApplicants().then(
			function(success) {
				console.log("getSideBarMenus :: success");
				if (success.data.status == 200) {
					return success.data.data;
				} 
			}, function(error) {});
	}
	
	this.autoUpdateStatus = function(appId) {
		return httpService.post(URLS.user + "/application/update_status_request",appId);
	};
	
	this.disbursmentSaved = function(appId, disbursmentDate) {
		var data = {};
		data.data = JSON.stringify({"applId":appId, "disbursmentDate":disbursmentDate});
		return httpService.post(URLS.user + "/application/save_disbursment", data.data);
	};
	
	this.pdiReportUpdate = function(appId, reportDate) {
		var data = {};
		data.data = JSON.stringify({"applId":appId, "reportedDate":reportDate});
		return httpService.post(URLS.user + "/application/update_pdi_report", data.data);
	};
	
	this.check30DaysGapFromLastTransaction = function() {
		return httpService.get(URLS.user + "/application/getDetailsOfGapBetweenLastTransaction");
	};
	
	this.validatePromoTionalCode = function(promoCode, applicationTypeId) {
		return httpService.get(URLS.user + "/application/validatePromoCode/"+promoCode + "/" + applicationTypeId);
	};
	
	this.getAllForAgencies = function() {
		return httpService.get(URLS.user + "/application/getAllForAgencies");
	};
	
}]);
