angular.module("lams").controller("sideBarCtrl",["$scope","$rootScope","Constant","$state","Notification","applicationService",
		function($scope,$rootScope,Constant,$state,Notification, applicationService) {
	
	$scope.coApplicantList = [];
	
	$scope.getSideBarMenus = function() {
		applicationService.getCoApplicants().then(
			function(success) {
				console.log("getSideBarMenus :: success");
				if (success.data.status == 200) {
					$scope.coApplicantList = success.data.data;
				} 
			}, function(error) {});
	}
	$scope.getSideBarMenus();
	
	$scope.go = function(){
		if($rootScope.isEmpty($rootScope.user) || $rootScope.user.isProfileFilled != true){
			Notification.warning("Please Complete your Profile to add Clients");
			if($state.$current.name != "web.lams.cpProfile"){
				$state.go("web.lams.cpProfile");
			}
			return false;
		}
		$state.go("web.lams.clients");
	}
	
	$rootScope.$on('refresh-sidebar',function(event,data){
        // you can access 'data_if_any' as 'data' variable
        $scope.getSideBarMenus();
    });
}]);
