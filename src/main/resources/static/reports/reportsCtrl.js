angular.module("lams").controller("reportsCtrl", [ "$scope", "$rootScope", "Constant","payoutService","documentService",
	function($scope, $rootScope, Constant, payoutService, documentService) {

		$scope.reportDataList = [];
		$scope.getReportForNonAdmin = function() {
			payoutService.getReportForNonAdmin().then(
				function(success) {
					if (success.data.status == 200) {
						$scope.reportDataList = success.data.data;
						$scope.reportDocumentList([ Constant.documentType.REPORT_DOCUMENT]);
					} else if (success.data.status == 400) {
						Notification.error(success.data.message);
					} else {
						Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
					}
				}, function(error) {
					console.log("error==>>", error);
					$rootScope.validateErrorResponse(error);
				});
		}
		$scope.getReportForNonAdmin();
		
		$scope.reportDocumentList = [];
		$scope.reportDocumentList = function(listOfDocumentMstId) {
			documentService.getReportDocuments(listOfDocumentMstId).then(
				function(success) {
					if (success.data.status == 200) {
						$scope.reportDocumentList = success.data.data;
					} else {
						Notification.warning(success.data.message);
					}
				}, function(error) {
					$rootScope.validateErrorResponse(error);
				});
		}
		
	} ]);