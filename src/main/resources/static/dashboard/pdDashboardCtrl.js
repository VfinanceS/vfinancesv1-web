angular.module("lams").controller("pdDashboardCtrl",["$scope", "$rootScope","Constant","applicationService", "$filter",
		function($scope,$rootScope,Constant,applicationService, $filter) {

	
	$scope.getProducts = function() {
		applicationService.getAll().then(
			function(success) {
				if (success.data.status == 200) {
					if(success.data.data != null){
						$scope.products = success.data.data[0];
						$scope.currentUser = success.data.data[1];
					}
					
					if($scope.currentUser.userType == Constant.UserType.PD_USER.id){
						
					}
					
					// When one product is available then get details automatically
					if($scope.products && $scope.products.length === 1){
						$scope.getBorrowerForLenderByApplicationId($scope.products[0],Constant.Status.FORMAPPLIED);
					} 
				} else {
					Notification.warning(success.data.message);
				}
			}, function(error) {
				$rootScope.validateErrorResponse(error);
			});
	}
	$scope.getProducts();
	
	$scope.borrowers = [];
	$scope.getBorrowers = function() {
		applicationService.getBorrowerForLender().then(
			function(success) {
				if (success.data.status == 200) {
					if(!$rootScope.isEmpty(success.data.data)){
						$scope.borrowers = success.data.data;
						console.log("$scope.users==>", $scope.borrowers);						
					}
				} else if (success.data.status == 400) {
					Notification.error(success.data.message);
				} else {
					Notification.error(Constant.ErrorMessage.SOMETHING_WENT_WRONG);
				}
			}, function(error) {
				$rootScope.validateErrorResponse(error);
			});
	}
	
	$scope.getBorrowers();
	
	$scope.getBorrowerForLenderByApplicationId = function(product,status) {
		product.isActiveTab = true
		applicationService.getBorrowerForPDUserByApplicationId(product.applicationTypeMstrBO.id,status).then(
			function(success) {
				if (success.data.status == 200) {
					if(status === Constant.Status.OPEN){
						product.applications = $filter("filter")(success.data.data,{isLoanDetailsLock : true});
					}else{
						product.applications = success.data.data;
						product.applications = _.uniqBy(product.applications, function (obj) {
							return obj.application.id;
							});
					}
				} else {
					Notification.warning(success.data.message);
				}
			}, function(error) {
				$rootScope.validateErrorResponse(error);
			});
	};

}]);
