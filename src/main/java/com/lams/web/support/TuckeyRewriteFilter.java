package com.lams.web.support;

import java.io.InputStream;
import java.net.URL;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.tuckey.web.filters.urlrewrite.Conf;

public class TuckeyRewriteFilter  extends org.tuckey.web.filters.urlrewrite.UrlRewriteFilter{
	@Override
    protected void loadUrlRewriter(FilterConfig filterConfig) throws ServletException {
        String confPath = filterConfig.getInitParameter("confPath");
        ServletContext context = filterConfig.getServletContext();
        try {
            final URL confUrl = getClass().getClassLoader().getResource(confPath);
            final InputStream config = getClass().getClassLoader().getResourceAsStream(confPath);
            Conf conf = new Conf(context, config, confPath, confUrl.toString(), false);
            checkConf(conf);
        } catch (Throwable e) {
            throw new ServletException(e);
        }
    }
}
