package com.lams.web.boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lams.web.support.TuckeyRewriteFilter;

@ComponentScan(basePackages = { "com.lams" })
@SpringBootApplication
public class LamsWebMain {
	
	public static final String REWRITE_FILTER_NAME = "rewriteFilter";
	public static final String REWRITE_FILTER_CONF_PATH = "urlrewrite.xml";

	@Autowired
	ApplicationContext applicationContext;

	public static void main(String[] args) {
		SpringApplication.run(LamsWebMain.class, args);
	}

	@Bean
	public ObjectMapper createObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		applicationContext.getAutowireCapableBeanFactory().autowireBean(objectMapper);
		return objectMapper;
	}
	
	@Bean
	public FilterRegistrationBean rewriteFilterConfig() {
		FilterRegistrationBean reg = new FilterRegistrationBean();
		reg.setName(REWRITE_FILTER_NAME);
		reg.setFilter(new TuckeyRewriteFilter());
		reg.addInitParameter("confPath", REWRITE_FILTER_CONF_PATH);
		reg.addInitParameter("confReloadCheckInterval", "-1");
		reg.addInitParameter("statusPath", "/redirect");
		reg.addInitParameter("statusEnabledOnHosts", "*");
		reg.addInitParameter("logLevel", "WARN");
		return reg;
	}

}
